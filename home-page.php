<?php /* Template Name: Home Page */ ?>
<?php get_header(); ?>

</div>
<div id="slider" class="container-fluid">
<?php if ( is_active_sidebar( 'home-slider-area' ) ) : ?>
        <?php
            if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('home-slider-area') ) :
                endif; ?>
        <?php endif; ?>
</div>

  <section id="featured-items" class="">
		<div class="container">
  		
		<div class="row">
				<div class="col-sm-3 col-xs-12">
					<div class="row">
						<div class="col-xs-12">
							<a href="<?php the_field('photo_link_1'); ?>" target="_self" class="">
  							
								<?php 
  								$image_1 = get_field('home_photo_1');
  								  if( !empty($image_1) ): ?>
  								  <img src="<?php echo $image_1['url']; ?>" alt="<?php echo $image_1['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_1'); ?></span>
							</a>
						</div>
					</div>
					<div class="row">
						<div class="col-xs-12">
							<a href="<?php the_field('photo_link_2'); ?>" target="_self" class="">
  							
								<?php 
  								$image_2 = get_field('home_photo_2');
  								  if( !empty($image_2) ): ?>
  								  <img src="<?php echo $image_2['url']; ?>" alt="<?php echo $image_2['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_2'); ?></span>
							</a>						</div>
					</div>
				</div>
				
				<div class="col-sm-3 col-xs-12">
					<a href="<?php the_field('photo_link_3'); ?>" target="_self" class="">
  							
								<?php 
  								$image_3 = get_field('home_photo_3');
  								  if( !empty($image_3) ): ?>
  								  <img src="<?php echo $image_3['url']; ?>" alt="<?php echo $image_3['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_3'); ?></span>
							</a>				
				  </div><?php /* 4 */ ?>
				  
				<div class="col-sm-6 col-xs-12">
					<div class="row">
						<div class="col-sm-6 col-xs-12">
							<a href="<?php the_field('photo_link_4'); ?>" target="_self" class="">
  							
								<?php 
  								$image_4 = get_field('home_photo_4');
  								  if( !empty($image_4) ): ?>
  								  <img src="<?php echo $image_4['url']; ?>" alt="<?php echo $image_4['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_4'); ?></span>
							</a>						
						</div>
						
						<div class="col-sm-6 col-xs-12">
							<a href="<?php the_field('photo_link_5'); ?>" target="_self" class="">
  							
								<?php 
  								$image_5 = get_field('home_photo_5');
  								  if( !empty($image_5) ): ?>
  								  <img src="<?php echo $image_5['url']; ?>" alt="<?php echo $image_5['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_5'); ?></span>
							</a>
							</div>
					</div>
					
					<div class="row">
						<div class="col-sm-12">
							<a href="<?php the_field('photo_link_6'); ?>" target="_self" class="">
  							
								<?php 
  								$image_6 = get_field('home_photo_6');
  								  if( !empty($image_6) ): ?>
  								  <img src="<?php echo $image_6['url']; ?>" alt="<?php echo $image_6['alt']; ?>" />
                <?php endif; ?>
                
								<span><?php the_field('photo_title_6'); ?></span>
							</a>					
							</div>
					</div>
					
				</div> <?php /* 4 */ ?>
				
			</div><?php /* row */ ?>

		</div>
	</section>
	
<div class="center-block">	
  <h3 class="box-title">Blog</h3>
</div>	

<div class="container-fluid blog-content">
  <div class="container">
  <div class="row">
  <?php
  
    $args = array(
      'post_type'=> 'post',
      'posts_per_page' => 4,
      'orderby' => 'post_date',
      'order' => 'DESC',
    );
      
    $recent_posts = new WP_Query($args);
  ?>
           
  <?php while ($recent_posts->have_posts()) : $recent_posts->the_post(); ?>
    <div class="col-md-3 col-sm-3 col-xs-6">
     <article <?php post_class(); ?>>
        <div class="article-info">
    		  <?php if ( has_post_thumbnail() && ! post_password_required() && ! is_attachment() ) : ?>
    				<div class="entry-thumbnail" <?php look_schema_metadata( array( 'context' => 'image' ) ); ?>>
    				  <a href="<?php the_permalink(); ?>">
    						<?php the_post_thumbnail( 'full' );  ?>
                </a>
    				</div>
            <?php endif; ?>
    				
    				<h2 class="post-title" itemprop="name"><a href="<?php the_permalink(); ?>" itemprop="url"><?php the_title(); ?></a></h2>
    				
    				<div class="article-description">
              <?php the_content( wp_kses(__( 'Read more <span>&rarr;</span>', 'look' ),array('span')) );?>
            </div>
      </article>  
    </div>
  <?php endwhile; ?>
</div>

<?php wp_reset_postdata(); ?>
</div>
</div>
</div>

<div class="container home-content hidden-sm hidden-xs">
  
  <div class="center-block">	
  <h3 class="box-title"><?php the_field('first_content_title'); ?></h3>
  </div>
  
	<div class="row">
    <div class="entry-content static-content"> 
  	<?php the_field('first_content_section'); ?>            
  	</div>
	</div>
	
</div>

<div class="container home-content hidden-sm hidden-xs">
  
  <div class="center-block">	
  <h3 class="box-title"><?php the_field('second_content_title'); ?></h3>
  </div>
  
	<div class="row">
    <div class="entry-content static-content"> 
  	<?php the_field('second_content_section'); ?>            
  	</div>
	</div>
	
</div>

<div class="container home-content hidden-sm hidden-xs">
  
  <div class="center-block">	
  <h3 class="box-title"><?php the_field('third_content_title'); ?></h3>
  </div>
  
	<div class="row">
    <div class="entry-content static-content"> 
  	<?php the_field('third_content_section'); ?>            
  	</div>
	</div>
	
</div>

<div id="main-content" class="container home-content hidden-sm hidden-xs">
	<div class="row">
				<div class="entry-content static-content">
					<?php the_content(); ?>                    
				</div>
			</article>
	</div>
</div>
<?php get_footer(); ?>