<?php

add_action( 'wp_enqueue_scripts', 'wpsites_load_parent_styles');
function wpsites_load_parent_styles() {
    wp_enqueue_style( 'parent-styles', get_template_directory_uri().'/style.css' );
}

if ( function_exists('register_sidebar') ) {
    register_sidebar(array(
    		'name' => __('Shop Widget', 'look'),
    		'id' => 'shop-sidebar',
    		'description' =>  __('Shop Widget Area','look'),
    		'before_widget' => '<div class="%1$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="text-heading"><h2 class="module-title"><span>',
    		'after_title' => '</span></h2></div>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Home Slider Widget', 'look'),
    		'id' => 'home-slider-area',
    		'description' =>  __('Slider Widget Area','look'),
    		'before_widget' => '<div class="%1$s">',
    		'after_widget' => '</div>',
    		'before_title' => '<div class="text-heading"><h2 class="module-title"><span>',
    		'after_title' => '</span></h2></div>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 1 Widget', 'look'),
    		'id' => 'footer-1-area',
    		'description' =>  __('Footer 1 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 1 Widget', 'look'),
    		'id' => 'footer-1-area',
    		'description' =>  __('Footer 1 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 2 Widget', 'look'),
    		'id' => 'footer-2-area',
    		'description' =>  __('Footer 2 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 3 Widget', 'look'),
    		'id' => 'footer-3-area',
    		'description' =>  __('Footer 3 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 4 Widget', 'look'),
    		'id' => 'footer-4-area',
    		'description' =>  __('Footer 4 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
    		
    		register_sidebar(array(
    		'name' => __('Footer 5 Widget', 'look'),
    		'id' => 'footer-5-area',
    		'description' =>  __('Footer 5 Widget Area','look'),
        'before_widget' => '<div id="%1$s" class="widget %2$s">',
        'after_widget'  => '</div>',
        'before_title' => '<h2>',
        'after_title' => '</h2>',
    		));
}

?>