<!DOCTYPE html>
<html <?php language_attributes(); ?> >
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="initial-scale=1, width=device-width, minimum-scale=1">
    <link rel="shortcut icon" type="image/png" href="<?php echo (look_get_option('look_favicon')=='')?  MAIN_ASSETS_URI.'/images/favicon.png': look_get_option('look_favicon') ; ?>"/>
    <?php wp_head(); ?>
</head>
<body <?php body_class( 'full-layout' ); look_schema_metadata( array( 'context' => 'body' ) ); ?>>


	<div id="wrapper">
		<?php if(look_get_option('header')== 0): ?>
			<header id="header" class="header header-layout-1" <?php look_schema_metadata( array( 'context' => 'header' ) ); ?>>
				<div class="header-top hidden-sm hidden-xs">
					<div class="container-fluid">
						<div class="row">
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								 <?php /* Remove/replace social placement
    								  look_social_block();  */ ?>
    								<ul class="search"> 
        							<li><a href="http://www.jackenglish.com/" target="_blank">Jack English</a> | </li>	
    								 <li><a href="http://www.surfimages.com/" target="_blank">SURFIMAGES</a></li>
    								</ul>
							</div>
							
							 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
    								<ul class="america"> 
        							<li> <img src="/wp-content/uploads/2016/08/usa.png" width="13" height="13" /> Products printed in America</li>
    								</ul>
							</div>
					
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12 top-menu">
								<?php if ( class_exists( 'WooCommerce' ) ) : ?>
									<?php
									global $woocommerce;
									$myaccount_page_id = get_option( 'woocommerce_myaccount_page_id' );
									$logout_url = '';
									if ( $myaccount_page_id ) {

										$logout_url = wp_logout_url( get_permalink( $myaccount_page_id ) );

										if ( get_option( 'woocommerce_force_ssl_checkout' ) == 'yes' )
											$logout_url = str_replace( 'http:', 'https:', $logout_url );
									}
									?>
									<ul>
										
										<?php /*serach moved to social placement above */ ?>
										
										<?php if ( class_exists( 'WooCommerce' ) ) : ?>
											<li class="cart icon">
											<a href="<?php echo esc_url($woocommerce->cart->get_cart_url()); ?>" title="<?php _e('Cart','look')?>">
                                                <?php _e('Shopping Cart','look');?>
                                                <!-- <span class="mini-cart-total-item"> -->
                                                   (<?php echo WC()->cart->cart_contents_count; ?>)
                                               <!--  </span> -->
                                            </a>
												<div class="hidden-sm hidden-xs">
													<?php the_widget('WC_Widget_Cart');?>
												</div>
											</li>
											
											<li class="cart">
											<a href="#"><a href="javascript:void(0);" title="<?php _e('Search','look')?>" data-target="#searchModal" data-toggle="modal"><?php _e('Search','look')?></a>
                                            </a>
											</li>
                                           
                                             <?php /*   
                                                <?php if(function_exists('yith_wcwl_count_products')):?>
												<li class="wishlist icon"><a href="<?php echo esc_url(  get_permalink(get_option( 'yith_wcwl_wishlist_page_id' )) ); ?>" title="<?php _e('Wishlist','look')?>"><?php _e('Wishlist','look'); ?><span class="wishlistcount"><?php echo yith_wcwl_count_products(); ?></span></a></li>
											<?php endif; ?>
											<li class="my-account <?php if (is_user_logged_in()):?> logged <?php endif; ?> icon"><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>" title="<?php _e('My Account','look'); ?>"><?php _e('My Account','look'); ?></a>
												<ul>
													<?php if (is_user_logged_in()):?>
														<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php _e('My Account','look'); ?></a></li>
														<li><a href="<?php echo (string)$logout_url; ?>"><?php _e('Logout','look'); ?></a></li>
													<?php else: ?>
														<li><a href="<?php echo get_permalink( get_option('woocommerce_myaccount_page_id') ); ?>"><?php _e('Register / Login','look'); ?></a></li>
													<?php endif; ?>
												</ul>
											</li>*/ ?>
											
										<?php endif ; ?>
										
									</ul>
								<?php endif; ?>
							</div>
						</div>
					</div>
				</div>
				
				<div class="container-fluid header-content">
				  <div class="row">
  				  
  			
  				  
						<div class="col-md-12">
								<h1 class="visual-hidden"><?php wp_title(); ?></h1>
								<div class="logo"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><img class="regular-logo" src="<?php echo (look_get_option('look_logo')=='')?  ASSETS_URI.'/images/logo.png': look_get_option('look_logo') ; ?>" alt="look"/><img class="retina-logo" src="<?php echo (look_get_option('look_retina_logo')=='')?  ASSETS_URI.'/images/logo2x.png': look_get_option('look_retina_logo') ; ?>" alt="look"/></a></div>
							</div>
							
							 
							
				  </div>
				</div>
			
								<?php wp_nav_menu( array( 'theme_location' => 'main-menu' ) ); ?>
							
			</header>
		
		
		<?php endif; ?>
		<?php look_get_template( 'title-bar' ); ?>
		<div id="content">


