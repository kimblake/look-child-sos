<?php
/**
 * Product Loop Start
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 */
?>
<div class="row row-inner">
<div class="col-lg-3 col-md-3 col-sm-4 col-xs-12 shop-sidebar">
    <?php if ( is_active_sidebar( 'shop-sidebar' ) ) : ?>
        <?php
            if ( !function_exists('dynamic_sidebar') || !dynamic_sidebar('shop-sidebar') ) :
                endif; ?>
        <?php endif; ?>

</div>   

    <div class="col-lg-9 col-md-9 col-sm-12 col-xs-12">
        <div class="theGrid">
                <ul class="grid row">